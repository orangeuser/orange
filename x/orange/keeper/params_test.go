package keeper_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	testkeeper "orange/testutil/keeper"
	"orange/x/orange/types"
)

func TestGetParams(t *testing.T) {
	k, ctx := testkeeper.OrangeKeeper(t)
	params := types.DefaultParams()

	k.SetParams(ctx, params)

	require.EqualValues(t, params, k.GetParams(ctx))
}
